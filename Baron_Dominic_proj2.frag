﻿// Baron_Dominic_proj2.frag
// Jessica Baron        CPSC 6050       Proj 2          Spring 2017
// Implement Phong shading with 3-point lighting per fragment here.
// Two textures are also applied to the color calculations.

varying vec3 eyeCoord_vNormal, eyeCoord_vPos;           //Interpolated values set by the vert shader.
uniform samplerCube tex0;
uniform samplerCube tex1;
in vec3 texCoords;

void main()
{
        vec3 P, N, L, V, H;
        vec4 diffuse = gl_FrontMaterial.diffuse;        //Through OpenGL, retrieve the material colors set in the main code.
        vec4 specular = gl_FrontMaterial.specular;      //      Multiply calculated lighting to these values.
        vec4 texColor0, texColor1, baseDiffuse; 
        vec4 totalLightDiff = {0.0, 0.0, 0.0, 0.0};             
        vec4 totalLightSpec = {0.0, 0.0, 0.0, 0.0};             
        float shininess = gl_FrontMaterial.shininess;
        float pi = 3.14159265;
        float normFactor = (shininess+2.0)/(pi*8.0);    //Phong normalizing factor for specular shading.
        float lightKey_fade = 1.0;
        float lightFill_fade = 1.0;
        float dist;     
        
//Calculate diffuse and specular for the key light.
        P = eyeCoord_vPos;      
        N = normalize(eyeCoord_vNormal);                //normalize() is a built-in GLSL function.
        L = normalize(gl_LightSource[0].position - P);  //Light vector for the KEY LIGHT. This light source position in eye coords.
        V = normalize(-P);                              //View vec = camera pos - view point, but camera pos at origin when using eye coords.
        H = normalize(L+V);                             //Halfway vector between light and view unit vectors.
        
        dist = distance(P, V);  
        lightKey_fade = 1.0 / (gl_LightSource[0].constantAttenuation +
                                        (gl_LightSource[0].linearAttenuation * dist) +
                                        (gl_LightSource[0].quadraticAttenuation * dist * dist) );       
        
        //The max assures no negative values, capping at 0.0. (Out of view.)
        totalLightDiff += (lightKey_fade * max(dot(N, L), 0.0) *gl_LightSource[0].diffuse);             
        totalLightSpec += (normFactor * pow(max(dot(H, N), 0.0), shininess) *gl_LightSource[0].specular);

//Calculate diffuse and specular for the FILL light. P, N, and V are the same since they're not dependent on the light.
        L = normalize(gl_LightSource[1].position - P);
        H = normalize(L+V);             

        lightFill_fade = 1.0 / (gl_LightSource[1].constantAttenuation +
                                        (gl_LightSource[1].linearAttenuation * dist) +
                                        (gl_LightSource[1].quadraticAttenuation * dist * dist) );       

        totalLightDiff += (lightFill_fade * max(dot(N, L), 0.0));               
        totalLightSpec += (normFactor*pow(max(dot(H, N), 0.0), shininess));       
        
//Calculate diffuse and specular for the BACK light.
        L = normalize(gl_LightSource[2].position - P);
        H = normalize(L+V);             
        totalLightDiff += max(dot(N, L), 0.0);  
        totalLightSpec += (normFactor*pow(max(dot(H, N), 0.0), shininess));       

//Final output. 
        texColor0 = texture(tex0, texCoords);   
        texColor1 = texture(tex1, texCoords);   
        baseDiffuse = (0.1 * diffuse) + (0.7 * texColor0) + (0.2 * texColor1);          //Diffuse before lighting is mainly the first texture, some of the environment texture, and a little bit of the base diffuse color.
        gl_FragColor = gl_LightSource[0].ambient + (baseDiffuse  * totalLightDiff) + (specular * totalLightSpec);       
}

