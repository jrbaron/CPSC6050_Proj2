// Baron_Dominic_proj2.vert
// Jessica Baron	CPSC 6050	Proj 2		Spring 2017

varying vec3 eyeCoord_vNormal, eyeCoord_vPos;			//Interpolated normals and positions between vertices
out vec3 texCoords;						//Generate texture coordinates here to pass to the frag shader.

void main()
{
	eyeCoord_vNormal = gl_NormalMatrix * gl_Normal;				//gl_NormalMatrix OpenGL takes from the ModelView mat.
	eyeCoord_vPos = gl_ModelViewMatrix * gl_Vertex;
	texCoords = reflect(-eyeCoord_vPos, normalize(eyeCoord_vNormal));	//GLSL built-in function reflect calculates reflection vector using the eye direction (- vertex pos) and surface normal.
	gl_Position = gl_ProjectionMatrix*gl_ModelViewMatrix*gl_Vertex;		//gl_Position is necessary output of the vert shader.
}
