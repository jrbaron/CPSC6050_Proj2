// Baron_Dominic_proj2.c
// Jessica Baron, James Dominic
// CPSC 6050, Spring 2017, Clemson University
// Project 2: "Show Me the Bunny" (GLSL and Phong shading)
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glx.h>
#include <GL/glext.h>
#include <stdio.h>
#include <stdlib.h>	//malloc(), calloc(), random()
#include <fcntl.h>	//basic open(), read(), write(), close()
#include <string.h>	//to help with atoi() with fgets()
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

GLuint texCube;

struct point
{
	float x;
	float y;
	float z;
};


//Strings for vertex arrays
GLfloat *bun_vertex = NULL;
GLfloat *bun_normal = NULL;
GLfloat *bun_faces = NULL;
int vertex_no, face_no, i;
GLfloat vertices[35947*6];

GLfloat *finalBuf = NULL;

/*
//eye, viewPt, and upVec used for gluLookAt().
float eye[] = {3.0, 3.0, 3.0};		
float viewPt[] = {0.0, 0.0, 0.0};
float upVec[] = {0.0, 1.0, 0.0};
*/

//-------------------------------------------------------------------------
//Reads the content of a file byte by byte. Used in compiling the shader programs.
char* readShader(char *filename)
{
        FILE *filePointer;
        char *content = NULL;
        int fileDescriptor, fileSize;   
        fileDescriptor = open(filename, O_RDONLY);      //Descriptor indexes into the file.
        fileSize = lseek(fileDescriptor, 0, SEEK_END);  //Seek to end of file. Size is in bytes.
        content = (char *) calloc(1,(fileSize+1));      //Allocate space as bytes needed plus one for the null char at end of string.
        lseek(fileDescriptor, 0, SEEK_SET);             //Set descriptor to the front of the file again, preparing to read.
        
     //Read fileSize*charSize bytes *into* content using the descriptor.
     //Returns the number of bytes read. (If this value is 0, something went wrong.)
        fileSize = read(fileDescriptor, content, fileSize*sizeof(char));
        content[fileSize] = '\0';                       //Null character at end of string.
        close(fileDescriptor);
        return content;
}

//-------------------------------------------------------------------------
//Reads a PLY file into a char array
void readPlyFile(char *filename)
{
	float scale = 0.1;

	FILE *fptr;
	char buf[512];
	//int *bun_faces = NULL;
	char *parse, *xval, * yval, *zval, *xnval, *ynval, *znval, *vert0, *vert1, *vert2;
	int vert0_int, vert1_int, vert2_int, firstVert;


	fptr=fopen(filename,"r");

	fgets(buf,512,fptr);	// strip off ply
	fgets(buf,512,fptr);	// format
	fgets(buf,512,fptr);	// comment

	fgets(buf,512,fptr);	// vertex number
	parse = strtok(buf," \t");
	parse = strtok(NULL," \t");
	parse = strtok(NULL," \t\n");
	vertex_no = atoi(parse);
	bun_vertex = (GLfloat *) calloc(vertex_no,6*sizeof(GLfloat)); // allocate memory for vertex and normals
	//bun_vertex = (GLfloat *) calloc(vertex_no,3*sizeof(GLfloat)); // allocate memory for vertex and normals
	//bun_normal= (GLfloat *) calloc(vertex_no,3*sizeof(GLfloat)); // allocate memory for vertex and normals

	fgets(buf,512,fptr);	// x
	fgets(buf,512,fptr);	// y
	fgets(buf,512,fptr);	// z
	fgets(buf,512,fptr);	// nx
	fgets(buf,512,fptr);	// ny
	fgets(buf,512,fptr);	// nz

	fgets(buf,512,fptr);	// face number
	parse = strtok(buf," \t");
	parse = strtok(NULL," \t");
	parse = strtok(NULL," \t\n");
	face_no = atoi(parse);
	//bun_faces = (int *) calloc(face_no,3*sizeof(int));

	fgets(buf,512,fptr);	// property list
	fgets(buf,512,fptr);	// end of comments


/*
	vertNormBuffer = (GLfloat *) calloc(vertex_no,6*sizeof(GLfloat)); // allocate memory for vertex and normals
        fread(vertNormBuffer,6,vertex_no,fptr);

	faceBuffer = (GLubyte *) calloc(face_no, 3*sizeof(GLubyte));
	fread(faceBuffer, 3, face_no, fptr);
        fclose(fptr);
*/

	// Strip vertex and normals
	for(i=0;i<vertex_no;i++)
	{
		fgets(buf,512,fptr);
		xval = strtok(buf," \t");
		yval = strtok(NULL," \t");
		zval = strtok(NULL," \t");
		xnval = strtok(NULL," \t");
		ynval = strtok(NULL," \t");
		znval = strtok(NULL," \n");

//		bun_vertex[3*i] = scale*atof(xval);
//		bun_vertex[3*i+1] = scale*atof(yval);
//		bun_vertex[3*i+2] = scale*atof(zval);
//		bun_normal[3*i] = atof(xnval);
//		bun_normal[3*i+1] = atof(ynval);
//		bun_normal[3*i+2] = atof(znval);

		bun_vertex[6*i] = atof(xval);
		bun_vertex[6*i+1] = atof(yval);
		bun_vertex[6*i+2] = atof(zval);
	//	bun_vertex[6*i+3] = atof(xnval);
	//	bun_vertex[6*i+4] = atof(ynval);
	//	bun_vertex[6*i+5] = atof(znval);
		bun_vertex[(3*vertex_no)+3*i] = atof(xnval);
		bun_vertex[(3*vertex_no)+3*i+1] = atof(ynval);
		bun_vertex[(3*vertex_no)+3*i+2] = atof(znval);
	}

/*
		printf("xval: %f \n", bun_vertex[0]);
		printf("yval: %f \n", bun_vertex[1]);
		printf("zval: %f \n", bun_vertex[2]);
		printf("xnval: %f \n", bun_vertex[]);
		printf("ynval: %f \n", bun_vertex[]);
		printf("znval: %f \n\n", bun_vertex[]);
*/


	finalBuf = (GLfloat *) calloc(face_no, 3*6*sizeof(GLfloat));	//each coord and normal for each vertex of each face

	// Strip face info
	for(i=0;i<face_no;i++)
	{
		fgets(buf,512,fptr);
		parse = strtok(buf," \t");
		vert0 = strtok(NULL," \t");
		vert1 = strtok(NULL," \t");
		vert2 = strtok(NULL," \n");
	//	bun_faces[3*i] = atoi(xface);
	//	bun_faces[3*i+1] = atoi(yface);
	//	bun_faces[3*i+2] = atoi(zface);

		vert0_int = atoi(vert0);
		vert1_int = atoi(vert1);
		vert2_int = atoi(vert2); 

		//firstVert =	//TODO: to help make indexing into the finalBuf more readable 

		finalBuf[3*i] = bun_vertex[vert0_int];
		finalBuf[3*i +1] = bun_vertex[vert0_int +1];
		finalBuf[3*i +2] = bun_vertex[vert0_int +2];
		finalBuf[(3*3*face_no)] = bun_vertex[3*vert0_int];
		finalBuf[(3*3*face_no) +1] = bun_vertex[(3*vert0_int) +1];
		finalBuf[(3*3*face_no) +2] = bun_vertex[(3*vert0_int) +2];

		finalBuf[3*i] = bun_vertex[vert1_int];
		finalBuf[3*i +1] = bun_vertex[vert1_int +1];
		finalBuf[3*i +2] = bun_vertex[vert1_int +2];
		finalBuf[(3*3*face_no)] = bun_vertex[3*vert1_int];
		finalBuf[(3*3*face_no) +1] = bun_vertex[(3*vert1_int) +1];
		finalBuf[(3*3*face_no) +2] = bun_vertex[(3*vert1_int) +2];
x
		finalBuf[3*i] = bun_vertex[vert2_int];
		finalBuf[3*i +1] = bun_vertex[vert2_int +1];
		finalBuf[3*i +2] = bun_vertex[vert2_int +2];
		finalBuf[(3*3*face_no)] = bun_vertex[3*vert2_int];
		finalBuf[(3*3*face_no) +1] = bun_vertex[(3*vert2_int) +1];
		finalBuf[(3*3*face_no) +2] = bun_vertex[(3*vert2_int) +2];
	}

	fclose(fptr);
	
	///
	
	
	
	
/*	
	for(i=0;i<vertex_no*3;i++)
	{
		vertNormBuffer[i] = bun_vertex[i];
		printf("vert: %f\n", vertices[i]);
	}

	for(i=vertex_no*3;i<vertex_no*6;i++)
	{
		vertNormBuffer[i] = bun_normal[i];
	//	printf("norm: %f\n", vertices[i]);
	}
*/

//	cfree(bun_vertex);	

/*
	int i;
	for(i=0; i<vertex_no; i++)
	{
		printf("vertex: %d", i);
		printf("x: %f\n", vertNormBuffer[i]);
	}
*/
	return;	
}
//-------------------------------------------------------------------------
//Read, compile, and link vertex and fragment shader programs.
//Returns the OpenGL program linked to the shaders.
unsigned int setShaders(char *vertFile, char *fragFile)
{
	printf("Setting shaders");
	GLint vertCompiled, fragCompiled;
	char *vertContent, *fragContent;
	GLuint vertShader, fragShader, prog;
	vertContent = readShader(vertFile);
	fragContent = readShader(fragFile);
	
	vertShader = glCreateShader(GL_VERTEX_SHADER);		//returns an int
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	//1 = length of string array, vert content. Load the content into the shader via pointer to a char array (pointer). NULL means all strings end in null char ('\0').
	glShaderSource(vertShader, 1, (const char **)&vertContent, NULL);
	glShaderSource(fragShader, 1, (const char **)&fragContent, NULL);
	free(vertContent);	//Not using original read content anymore.
	free(fragContent);
	glCompileShader(vertShader);
	glCompileShader(fragShader);
	prog = glCreateProgram();
	glAttachShader(prog, vertShader);
	glAttachShader(prog, fragShader);
	glLinkProgram(prog);		//What actually intercepts any of the fixed function OpenGL code.
	return(prog);
}

//-------------------------------------------------------------------------
void loadTexture(char *filename)
{
	FILE *fopen(), *fptr;
	char buf[512];
	int imgSize, imgWidth, imgHeight, maxColor;
	unsigned char *texBytes, *parse;
	GLubyte *glBytes;

	fptr=fopen(filename,"r");
	fgets(buf,512,fptr); 	//Strip first line.

	do			//Strip off comments.
	{ fgets(buf,512,fptr); }
	while(buf[0]=='#');

	parse = strtok(buf," \t");
	imgWidth = atoi(parse);
	printf("\n width: %d", imgWidth);

	parse = strtok(NULL," \n");
	imgHeight = atoi(parse);
	printf("\n height: %d", imgHeight);

	fgets(buf,512,fptr); 
	parse = strtok(buf," \n");
	maxColor = atoi(parse);
	printf("\n max color: %d", maxColor);

	imgSize = imgWidth*imgHeight;
	texBytes = (unsigned char *)calloc(3,imgSize);
	fread(texBytes,3,imgSize,fptr);
	fclose(fptr);

	//TODO: Testing texture binding	
	glBytes = (GLubyte*) malloc(3*imgWidth*imgHeight);
	int i;
	for(i=0; i<3*imgWidth*imgHeight; i++)
	{
		if (i % 3 == 0)
		{ glBytes[i] = 255; }
		else
		{ glBytes[i] = 0; }
	}

/*
	//GLfloat plane[] = {0.0, 0.0, 0.0, 0.0};
	//glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
	//glTexGenfv(GL_S, GL_OBJECT_PLANE, plane);	
	//glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
	//glTexGenfv(GL_T, GL_OBJECT_PLANE, plane);	

	glBindTexture(GL_TEXTURE_2D,1);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,imgWidth,imgHeight,0,GL_RGB,
        	GL_UNSIGNED_BYTE,texBytes);
//	glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,64,64,0,GL_RGB,
 //       	GL_UNSIGNED_BYTE,glBytes);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
//	cfree(glBytes);	

*/

	//Cube map to surround the object. Texture coords come from intersecting obj normals with a cube face.
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &texCube);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texCube);
//	glTexStorage2D(GL_TEXTURE_CUBE_MAP, 10, GL_RGB, 1024, 1024);	//Calls glTexImage2D per face with NULL as data.

	int face;
	for(face=0; face<6; face++)
	{
		//Each face of the cube map is a separate 2D image, indexing starting with the positive X face.
//		glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face,
//			0, 0, 0,				//mipmap level, x offset, y offset
//			1024, 1024, GL_RGB,			//dimensions and format
//			GL_UNSIGNED_BYTE, texBytes);		//type and data
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face,
			0, GL_RGB, imgWidth, imgHeight, 0, GL_RGB,
			GL_UNSIGNED_BYTE, texBytes);
	} 

	free(texBytes);
	free(glBytes);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

//-------------------------------------------------------------------------
void setupViewVolume()
{

struct point eye;
struct point view;
struct point up;

glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);			//Set current matrix as projection matrix.
	glLoadIdentity();								//Set proj mat as the identity matrix.
	gluPerspective(45.0, 1.0, 1.0, 20.0);	//45 degree FOV, aspect ratio=1, near CP=1, farCP=20
	
	glMatrixMode(GL_MODELVIEW);			//Now set ModelView mat.
	glLoadIdentity();

	eye.x = 3.0; eye.y = 3.0; eye.z = 3.0;
	view.x = 0.0; view.y = 0.0; view.z = 0.0;
	up.x = 0.0; up.y = 1.0; up.z = 0.0;

	gluLookAt(eye.x,eye.y,eye.z,view.x,view.y,view.z,up.x,up.y,up.z);
}

//-------------------------------------------------------------------------
void setLights()
{
	float lightKey_pos[] = {10.0, 12.0, 6.0, 1.0};		//The key light. To the above, right, and behind the eye.
	float lightFill_pos[] = {-8.0, 12.0, 11.0, 1.0};	//The fill light. Angled opposite of key but not symmetric, and lower.
	float lightBack_pos[] = {-10.0, -12.0, -6.0, 1.0};	//The back light. Opposite camera and behind mesh.

	float lightKey_ambient[] = {0.0, 0.0, 0.0, 0.0};
	float lightKey_diffuse[] = {2.0, 2.0, 2.0, 0.0};
	float lightKey_specular[] = {2.2, 2.2, 2.2, 0.0};
	float lightKey_direction[] = {-10.0, -12.0, -6.0, 1.0};

	glMatrixMode(GL_MODELVIEW);		//Make sure to set this so glLightfv() can create eye coords.
	glLightfv(GL_LIGHT0, GL_POSITION, lightKey_pos);
	glLightfv(GL_LIGHT1, GL_POSITION, lightFill_pos);
	glLightfv(GL_LIGHT2, GL_POSITION, lightBack_pos);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightKey_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKey_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightKey_specular);
//	glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 1.0);
//	glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 80.0);
//	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightKey_direction);
	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1.0);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.075);
}

//-------------------------------------------------------------------------
void setMaterial()		//of the bunny
{
	float diffuse[] = {0.35, 0.35, 0.5, 1.0};		//RGBA of a medium blue.
	float specular[] = {0.1, 0.85, 0.75, 1.0};		//Hmm, let's see how a slightly unsaturated cyan highlight looks!
	float shininess[] = {10.0};
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
}

//-------------------------------------------------------------------------
void setUniformParams(unsigned int prog)
{
	//Setting values from .c to variables in the shader program.
	int location;
//	location = glGetUniformLocation(prog, "texture");
	location = glGetUniformLocation(prog, "cubeTex");
	glUniform1i(location, texCube);
//	glUniform1i(location, 0);		//Set uniform var "texture" with value of ID# 0.
//	glUniform1i(location, 1);		//Set uniform var "texture" with value of ID# 0.
}

//-------------------------------------------------------------------------
void renderScene(void)		//Called through gluDisplayFunc().
{
	setMaterial();
//	glEnable(GL_DEPTH_TEST);
	glClearColor(0.32, 0.3, 0.32, 1.0);		//Medium gray slightly purple
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texCube);
//	glBindTexture(GL_TEXTURE_2D, 1);

	setupViewVolume();
//	glutSolidTeapot(1.0);		//for testing




        //glVertexPointer(3, GL_FLOAT, 3*sizeof(GLfloat), NULL+0);        //The last param is changed into an *address,* so having NULL there tricks OpenGL.
        //glVertexPointer(3, GL_FLOAT, 3*sizeof(GLfloat), vertices);     
        //glVertexPointer(3, GL_FLOAT, 3*sizeof(GLfloat), bun_vertex);     
       // glNormalPointer(GL_FLOAT, 3*sizeof(GLfloat), NULL+ 3*vertex_no*sizeof(GLfloat));
        //glNormalPointer(GL_FLOAT, 3*sizeof(GLfloat), &vertNormBuffer[3]);
       // glNormalPointer(GL_FLOAT, 3*sizeof(GLfloat), bun_normal);

	//glDrawArrays(GL_QUADS, 0, vertex_no);	//index of first, num of indices (vertices) specified, of the currently bound buffer (set in main).
//	glDrawElements(GL_TRIANGLES, face_no, GL_UNSIGNED_BYTE, faceBuffer);

        glBindBuffer(GL_ARRAY_BUFFER, 1);
        glBufferData(GL_ARRAY_BUFFER, sizeof(finalBuf), finalBuf, GL_STATIC_DRAW);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_NORMAL_ARRAY);
        glVertexPointer(3, GL_FLOAT, 3*sizeof(GLfloat), NULL+0);     
        glNormalPointer(GL_FLOAT, 3*sizeof(GLfloat), NULL+ face_no*3*3*sizeof(GLfloat)); //normals start halfway through the buf
											//which is after the 3 pos coords per vert per face. Now start the 3 norm coords per vert per face. 

	glDrawArrays(GL_TRIANGLES, 0, face_no*3);	//Draw verts in order of how they're used for the faces. 




/*
    // VAO
    GLuint vao;
    glGenVertexArrays (1, &vao);
    glBindVertexArray (vao);
 
    // VBO for Coords
    GLuint points_vbo;
    glGenBuffers(1, &points_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
    glBufferData(GL_ARRAY_BUFFER, 3*vertex_no*sizeof(GLfloat), bun_vertex, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray (0);
 
    // VBO for Normals (needed for light/shading surface calcuation)
    GLuint normals_vbo;
    glGenBuffers(1, &normals_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, normals_vbo);
    glBufferData(GL_ARRAY_BUFFER, 3*vertex_no*sizeof(GLfloat), bun_normal, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray (1);

    glDrawArrays(GL_TRIANGLES, 0, vertex_no);
*/

	glutSwapBuffers();		//Double buffer rendering
}
//-------------------------------------------------------------------------
void quit(unsigned char key, int x, int y)	//Called through glutKeyboardFunc().
{
switch(key) 
{
        case 'q':
                exit(1);
        default:
                break;
    }
}
//-------------------------------------------------------------------------
int main(int argc, char **argv)
{
	int prog;
	
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH|GLUT_MULTISAMPLE);
	glutInitWindowSize(768,768);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Show Me the Bunny");

	loadTexture(argv[1]);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE_ARB);	//For antialiasing
	setupViewVolume();
	readPlyFile(argv[2]);
	setLights();
	setMaterial();
	prog = setShaders("Baron_Dominic_proj2.vert", "Baron_Dominic_proj2.frag");
	setUniformParams(prog);
	glUseProgram(prog);
	
	glutDisplayFunc(renderScene);		//Looping executions of renderSene()
	glutKeyboardFunc(quit);				//Keyboard actions go to quit().
	glutMainLoop();
	return 0;
}

