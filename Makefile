BIN = run
CC = gcc
FLAGS = -Wall -pedantic
INCLUDE = -I/usr/include -I/usr/X11R6/include
SRC = Baron_Dominic_proj2.c 
LIBRARIES = -L/usr/lib -L/usr/X11R6/lib -lX11 -lGL -lGLU -lglut -lm -lXmu -lXi 

all:
	${CC} ${FLAGS} -o ${BIN} ${SRC} ${INCLUDE} ${LIBRARIES}
	
clean:
	${BIN}
