// Baron_Dominic_proj2.c
// Jessica Baron, James Dominic
// CPSC 6050, Spring 2017, Clemson University
// Project 2: "Show Me the Bunny" (GLSL and Phong shading with textures added)

#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glx.h>
#include <GL/glext.h>
#include <stdio.h>
#include <stdlib.h>	//malloc(), calloc(), random()
#include <fcntl.h>	//basic open(), read(), write(), close()
#include <string.h>	//to help with atoi() with fgets()
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

struct point
{
	float x;
	float y;
	float z;
};

//These values are global because they're used in renderScene, which is set as the glutDisplayFunc.
GLfloat *final_vertices;
int face_no;

//-------------------------------------------------------------------------
//Reads the content of a file byte by byte. Used in compiling the shader programs.
char* readShader(char *filename)
{
        FILE *filePointer;
        char *content = NULL;
        int fileDescr, fileSize;   
        fileDescr = open(filename, O_RDONLY);      	//Descriptor indexes into the file.
        fileSize = lseek(fileDescr, 0, SEEK_END);  	//Seek to end of file. Size is in bytes.
        content = (char *) calloc(1,(fileSize+1));      //Allocate space as bytes needed plus one for the null char at end of string.
        lseek(fileDescr, 0, SEEK_SET);             	//Set descriptor to the front of the file again, preparing to read.
        
     //Read fileSize*charSize bytes into content using the descriptor.
     //Returns the number of bytes read. (If this value is 0, something went wrong.)
        fileSize = read(fileDescr, content, fileSize*sizeof(char));
        content[fileSize] = '\0';                       //Null character at end of string.
        close(fileDescr);
        return content;
}

//-------------------------------------------------------------------------
//Read, compile, and link vertex and fragment shader programs.
//Returns the OpenGL program linked to the shaders.
unsigned int setShaders(char *vertFile, char *fragFile)
{
	GLuint vertCompiled, fragCompiled, vertShader, fragShader, prog;
	char *vertContent, *fragContent;
	vertContent = readShader(vertFile);
	fragContent = readShader(fragFile);
	
	vertShader = glCreateShader(GL_VERTEX_SHADER);		//returns an int
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	//1 = length of string array, vert content. Load the content into the shader via pointer to a char array (pointer). NULL means all strings end in null char ('\0').
	glShaderSource(vertShader, 1, (const char **)&vertContent, NULL);
	glShaderSource(fragShader, 1, (const char **)&fragContent, NULL);
	free(vertContent);	//Not using original read content anymore.
	free(fragContent);
	glCompileShader(vertShader);
	glCompileShader(fragShader);
	prog = glCreateProgram();
	glAttachShader(prog, vertShader);
	glAttachShader(prog, fragShader);
	glLinkProgram(prog);		
	return(prog);
}

//-------------------------------------------------------------------------
//Reads a PLY file into a final vertex array.
void readPlyFile(char *filename)
{
	FILE *fptr;
	char buf[512];
	int *faces;
	GLfloat *orig_vertices, *orig_normals;
	float scale = 14.5;
	int vertex_no, i;
	char *parse, *xval, *yval, *zval, *xnval, *ynval, *znval, *v0, *v1, *v2;

	fptr=fopen(filename,"r");

	fgets(buf,512,fptr);	// strip off ply
	fgets(buf,512,fptr);	// format
	fgets(buf,512,fptr);	// comment

	fgets(buf,512,fptr);	// vertex number
	parse = strtok(buf," \t");
	parse = strtok(NULL," \t");
	parse = strtok(NULL," \t\n");
	vertex_no = atoi(parse);

	fgets(buf,512,fptr);	// x
	fgets(buf,512,fptr);	// y
	fgets(buf,512,fptr);	// z
	fgets(buf,512,fptr);	// nx
	fgets(buf,512,fptr);	// ny
	fgets(buf,512,fptr);	// nz

	fgets(buf,512,fptr);	// face number
	parse = strtok(buf," \t");
	parse = strtok(NULL," \t");
	parse = strtok(NULL," \t\n");
	face_no = atoi(parse);

	fgets(buf,512,fptr);	// property list
	fgets(buf,512,fptr);	// end of comments

	orig_vertices = (GLfloat *) calloc(vertex_no,3*sizeof(GLfloat)); // allocate memory for vertex and normals
	orig_normals = (GLfloat *) calloc(vertex_no,3*sizeof(GLfloat));
	faces = (int *) calloc(face_no,3*sizeof(int));

	//Save original vertex and normal values. 
	for(i=0;i<vertex_no;i++)
	{
		fgets(buf,512,fptr);
		xval = strtok(buf," \t");
		yval = strtok(NULL," \t");
		zval = strtok(NULL," \t");
		xnval = strtok(NULL," \t");
		ynval = strtok(NULL," \t");
		znval = strtok(NULL," \n");
		orig_vertices[3*i] = atof(xval);
		orig_vertices[3*i+1] = atof(yval);
		orig_vertices[3*i+2] = atof(zval);
		orig_normals[3*i] = atof(xnval);
		orig_normals[3*i+1] = atof(ynval);
		orig_normals[3*i+2] = atof(znval);
	}

	//Save the faces (groups of 3 vertices). 
	for(i=0;i<face_no;i++)
	{
		fgets(buf,512,fptr);
		parse = strtok(buf," \t");
		v0 = strtok(NULL," \t");
		v1 = strtok(NULL," \t");
		v2 = strtok(NULL," \n");
		faces[3*i] = atoi(v0);
		faces[3*i+1] = atoi(v1);
		faces[3*i+2] = atoi(v2);
	}

	fclose(fptr);			//Finished reading from the file.

	//The final vertices are those to actually render, based on the sequence they appear in the array of faces.
	//Vertex positions are in the first half of the final array and the normals in the second half.
	final_vertices = (GLfloat *) calloc(face_no, 3*3*2*sizeof(GLfloat));
	for(i=0;i<face_no*3;i++)
	{
		// Vertices
		final_vertices[3*i] = orig_vertices[(faces[i])*3] *scale;
		final_vertices[3*i+1] = orig_vertices[((faces[i])*3)+1] *scale;
		final_vertices[3*i+2] = orig_vertices[((faces[i])*3)+2] *scale;
		// Normals
		final_vertices[(face_no*3*3)+3*i] = orig_normals[(faces[i])*3];
		final_vertices[(face_no*3*3)+3*i+1] = orig_normals[((faces[i])*3)+1];
		final_vertices[(face_no*3*3)+3*i+2] = orig_normals[((faces[i])*3)+2];
	}

	cfree(orig_vertices);		//Done with these temporary arrays.	
	cfree(orig_normals);	
	cfree(faces);	
}

//-------------------------------------------------------------------------
void loadTexture(char *filename, unsigned int texID)
{
	FILE *fopen(), *fptr;
	char buf[512];
	int imgSize, imgWidth, imgHeight, maxColor, face;
	unsigned char *texBytes, *parse;

	fptr=fopen(filename,"r");
	fgets(buf,512,fptr); 	//Strip first line.

	do			//Strip off comments.
	{ fgets(buf,512,fptr); }
	while(buf[0]=='#');

	parse = strtok(buf," \t");
	imgWidth = atoi(parse);
	parse = strtok(NULL," \n");
	imgHeight = atoi(parse);

	fgets(buf,512,fptr); 
	parse = strtok(buf," \n");
	maxColor = atoi(parse);

	imgSize = imgWidth*imgHeight;
	texBytes = (unsigned char *)calloc(3,imgSize);
	fread(texBytes,3,imgSize,fptr);
	fclose(fptr);

	//Cube map to surround the object. Texture coords come from intersecting obj normals with a cube face.
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID);
	for(face=0; face<6; face++)
	{	//Each face of the cube map is a separate 2D image, indexing starting with the positive X face.
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face,
			0, GL_RGB, imgWidth, imgHeight, 0, GL_RGB,
			GL_UNSIGNED_BYTE, texBytes);
	} 

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 	//Texture magnification with small (>=1) area of texture element.
  	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	free(texBytes);
}

//-------------------------------------------------------------------------
//Set up the initial projection and modelview matrices of the scene.
void setupViewVolume()
{
	struct point eye, view, up;
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);			//Set current matrix as projection matrix.
	glLoadIdentity();								//Set proj mat as the identity matrix.
	gluPerspective(45.0, 1.0, 1.0, 100.0);	//45 degree FOV, aspect ratio=1, near CP=1, farCP=20
	
	glMatrixMode(GL_MODELVIEW);			//Now set ModelView mat.
	glLoadIdentity();
	eye.x = 1.0; eye.y = 3.5; eye.z = 4.0;
	view.x = -0.3; view.y = 1.6; view.z = 0.3;
	up.x = 0.0; up.y = 1.0; up.z = 0.0;
	gluLookAt(eye.x,eye.y,eye.z,view.x,view.y,view.z,up.x,up.y,up.z);
}

//-------------------------------------------------------------------------
//Set up the key, fill, and back lights and their properties to use in the shaders.
void setLights()
{
	float lightKey_pos[] = {-14.0, 15.0, 12.0, 1.0};	//The key light. To the above, left, and behind the eye.
	float lightFill_pos[] = {6.0, 10.0, 8.0, 1.0};		//The fill light. Angled opposite of key, but not symmetric, and lower.
	float lightBack_pos[] = {7.0, 2.5, -9.0, 1.0};		//The back light. Opposite camera and behind mesh.

	float lightKey_ambient[] = {0.0, 0.0, 0.0, 0.0};	//Ambient, diffuse, and specular properties of the key light.
	float lightKey_diffuse[] = {2.5, 2.5, 2.5, 0.0};
	float lightKey_specular[] = {1.2, 1.2, 1.2, 0.0};

	glMatrixMode(GL_MODELVIEW);				//Make sure to set this so glLightfv() can create eye-space coords for the light positions.
	glLightfv(GL_LIGHT0, GL_POSITION, lightKey_pos);
	glLightfv(GL_LIGHT1, GL_POSITION, lightFill_pos);
	glLightfv(GL_LIGHT2, GL_POSITION, lightBack_pos);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightKey_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKey_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightKey_specular);

	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1.0);	//Set fade constants for the key and fill lights.
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.08);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.05);
	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1.0);
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.1);
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.08);
	glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, 1.0);
	glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, 0.1);
	glLightf(GL_LIGHT2, GL_QUADRATIC_ATTENUATION, 0.08);
}

//-------------------------------------------------------------------------
//Set diffuse and specular color and shininess of the bunny's material to be accessed via shaders.
void setMaterial()	
{
	float diffuse[] = {0.35, 0.35, 0.5, 1.0};
	float specular[] = {0.2, 0.3, 0.3, 1.0};	
	float shininess[] = {25.0};
	
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
}

//-------------------------------------------------------------------------
//Set values from the GL code to uniform variables in the shader program.
void setUniformParams(unsigned int prog)
{
	glUseProgram(prog);
	int location;
	location = glGetUniformLocation(prog, "tex0");		//The main texture of the bunny's surface.
	glUniform1i(location, 0);
	location = glGetUniformLocation(prog, "tex1");		//The texture of the environment (background and reflections).
	glUniform1i(location, 1);		
}

//-------------------------------------------------------------------------
//Rendering function called through gluDisplayFunc().
void renderScene(void)		
{
	GLUquadric* qptr = gluNewQuadric();	
	
	glClearColor(0.32, 0.3, 0.32, 1.0);		//Medium gray slightly purple
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	gluQuadricTexture(qptr, 1);			//Generates texture coords for the quadric.
	gluQuadricOrientation(qptr, GLU_INSIDE);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 2);		//Bind the environment texture to the active texture for the quadric.
	gluSphere(qptr, 20.0, 64, 64);			//Large enough sphere to encompass the scene

	glActiveTexture(GL_TEXTURE0);			//Bunny material
	glBindTexture(GL_TEXTURE_CUBE_MAP, 1);
	
	glBindBuffer(GL_ARRAY_BUFFER, 1);
	glBufferData(GL_ARRAY_BUFFER, face_no*3*3*2*sizeof(GLfloat), final_vertices, GL_STATIC_DRAW);
	// When using VBOs, the final arg is a byte offset in buffer, not the address,
	// but gl<whatever>Pointer still expects an address type, hence the NULL.
	glVertexPointer(3,GL_FLOAT,3*sizeof(GLfloat),NULL+0);
	glNormalPointer(GL_FLOAT,3*sizeof(GLfloat),NULL+3*3*face_no*sizeof(GLfloat));
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glDrawArrays(GL_TRIANGLES, 0, face_no*3);	//Draw verts in order of how they're used for the faces.

	glutSwapBuffers();				//Double buffer rendering
}

//-------------------------------------------------------------------------
//Exit function called through glutKeyboardFunc().
void quit(unsigned char key, int x, int y)	
{
	switch(key) 
	{
        	case 'q':
               		exit(1);
        	default:
                	break;
    	}
}

//-------------------------------------------------------------------------
//The first cmd line arg is the PLY file, and the following two are PPM texture files.
int main(int argc, char **argv)
{
	int prog;
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH|GLUT_MULTISAMPLE);
	glutInitWindowSize(768,768);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Show Me the Bunny");

	loadTexture(argv[2], 1);
	loadTexture(argv[3], 2);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE_ARB);		//For antialiasing
	
	setupViewVolume();
	readPlyFile(argv[1]);
	setLights();
	setMaterial();
	prog = setShaders("Baron_Dominic_proj2.vert", "Baron_Dominic_proj2.frag");
	glUseProgram(prog);
	setUniformParams(prog);

	glutDisplayFunc(renderScene);		//Looping executions of renderSene()
	glutKeyboardFunc(quit);			//Keyboard actions go to quit().
	glutMainLoop();
	return 0;
}

